import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductsService } from '../../services/products.service';
import { IManufacturer, IProduct } from 'src/app/models/product';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-products-create',
  templateUrl: './products-create.component.html',
  styleUrls: ['./products-create.component.scss']
})
export class ProductsCreateComponent {

  productId: string | null = '0';
  manufacturers: IManufacturer[] = [];
  productForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
    price: new FormControl(0, [Validators.required, Validators.min(1)]),
    manufacturer: new FormControl('', [Validators.required]),
    expiryDate: new FormControl(<any>(null), [Validators.required]),
  });
  selectedManufacturerId: string | undefined;
  addEditFormSubmitted = false;

  constructor(private formBuilder: FormBuilder, private router: Router, private activatedRoute: ActivatedRoute,
    private productService: ProductsService) {
    this.manufacturers = this.productService.getAllManufacturers();
    this.activatedRoute.paramMap.subscribe((params) => {
      this.productId = params.get('id');
    });
    let productForEdit = this.productService.getProductById(this.productId!);
    if (this.productId != '0') {
      this.productForm = this.formBuilder.group({
        name: new FormControl(productForEdit?.name as string, [Validators.required]),
        price: new FormControl(productForEdit?.price as number, [Validators.required]),
        expiryDate:  new FormControl(formatDate(productForEdit?.expiryDate!, "yyyy-MM-dd", "en"), [Validators.required]),
        manufacturer: new FormControl(),
      });
      this.selectedManufacturerId = productForEdit?.manufacturer?.id;
    }
    if (this.selectedManufacturerId == undefined) {
      this.selectedManufacturerId = "";
    }
  }

  saveProduct() {
    this.addEditFormSubmitted = true;
    if (this.productForm.valid) {

      const name = this.productForm.value.name as string;
      const price = this.productForm.value.price as number;
      const expiryDate = this.productForm.value.expiryDate as Date;
      const manufacturer = this.productService.getManufacturerById(this.selectedManufacturerId)!;

      const updatedProduct: IProduct = {
        id: this.productId!,
        name: name,
        manufacturer: manufacturer,
        price: price,
        expiryDate: expiryDate
      }

      if (this.productId != '0') {
        this.productService.updateProduct(this.productId!, updatedProduct)
      }
      else {
        updatedProduct.id = (Math.random() * (100000)).toFixed(0).toString();
        this.productService.addProduct(updatedProduct)
      }   
       this.addEditFormSubmitted = false;
      this.router.navigate(['products']);
    }
  }

  back() {
    this.router.navigate(['products']);
  }
  onChangeManufacturer(event: any) {
    this.selectedManufacturerId = event.target.value;
  }

   //#region  Validation properties
  
   get productName() {
    return this.productForm.get('name');
  }

  get productPrice() {
    return this.productForm.get('price');
  }

  get productManufacturer() {
    return this.productForm.get('manufacturer');
  }

  get productExpiryDate() {
    return this.productForm.get('expiryDate');
  }

  //#endregion
}
