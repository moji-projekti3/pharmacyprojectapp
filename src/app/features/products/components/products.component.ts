import { Component } from '@angular/core';
import { ProductsService } from '../services/products.service';
import { IProduct } from 'src/app/models/product';
import { Router } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent {

  products: IProduct[] = []
  constructor(private router: Router, 
    private productServie: ProductsService) {

      this.refreshProducts();
  }
  
  createProduct(productId: string = "0"): void {
    this.router.navigate(['products-create/', productId]);
  }

  deleteProduct(productId: string){
    this.productServie.deleteProduct(productId)
    this.refreshProducts();
  }

  refreshProducts(){
    this.products = this.productServie.getAllProducts();
  }
}
