import { Injectable } from '@angular/core';
import { IManufacturer, IProduct } from 'src/app/models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {


  products: IProduct[] = [];
  manufacturers: IManufacturer[] = [];

  constructor() {

    const manufacturer1: IManufacturer = { id: "1", name: "Galenika" }
    const manufacturer2: IManufacturer = { id: "2", name: "Hemofarm" }
    const manufacturer3: IManufacturer = { id: "3", name: "Bayer" }
    const manufacturer4: IManufacturer = { id: "4", name: "DM" }
    const manufacturer5: IManufacturer = { id: "5", name: "Lilly" }

    this.manufacturers.push(manufacturer1, manufacturer2, manufacturer3,manufacturer4,manufacturer5);
    
    const product1: IProduct = {
      id: "1", name: "Brufen",
      manufacturer: manufacturer1,
      price: 450,
      expiryDate: new Date()
    };
    const product2: IProduct = {
      id: "2", name: "Aspirin",
      manufacturer: manufacturer2,
      price: 440,
      expiryDate: new Date()
    };

    const product3: IProduct = {
      id: "3", name: "Fervex",
      manufacturer: manufacturer3,
      price: 590,
      expiryDate: new Date()
    };
    const product4: IProduct = {
      id: "4", name: "Aspirin Cold",
      manufacturer: manufacturer3,
      price: 700,
      expiryDate: new Date()
    };
    const product5: IProduct = {
      id: "5", name: "Magnezium",
      manufacturer: manufacturer2,
      price: 660,
      expiryDate: new Date()
    };
    const product6: IProduct = {
      id: "6", name: "Vitamin D3",
      manufacturer: manufacturer4,
      price: 990,
      expiryDate: new Date()
    };
    const product7: IProduct = {
      id: "6", name: "Iboprufen",
      manufacturer: manufacturer5,
      price: 490,
      expiryDate: new Date()
    };


    this.products.push(product1, product2, product3,product4,product5,product6)
  }
  getAllProducts(): IProduct[] {
    return this.products;
  }

  getAllManufacturers(): IManufacturer[] {
    return this.manufacturers;
  }

  getProductById(productId: string): IProduct | undefined {
    return this.products.find((product) => product.id === productId);
  }

  getManufacturerById(selectedManufacturerId: string | undefined) {
    return this.manufacturers.find((manufacturer) => manufacturer.id === selectedManufacturerId);
  }

  addProduct(newProduct: IProduct): void {
    // Dodavanje novog proizvoda
    this.products.push(newProduct);
  }

  updateProduct(productId: string, updatedProduct: IProduct): void {
    let indexToUpdate = this.products.findIndex(item => item.id === updatedProduct.id);
    this.products[indexToUpdate] = updatedProduct;
  }

  deleteProduct(productId: string): void {
    // Brisanje proizvoda
    this.products = this.products.filter((product) => product.id !== productId);
  }
}
