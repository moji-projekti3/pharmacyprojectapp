import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductsComponent } from './features/products/components/products.component';
import { AboutComponent } from './features/about/about.component';
import { ProductsCreateComponent } from './features/products/components/products-create/products-create.component';

const routes: Routes = [
  { path: 'products', component: ProductsComponent },
  { path: 'products-create/:id', component: ProductsCreateComponent },
  { path: 'about', component: AboutComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
